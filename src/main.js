var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var array
var tileDimension = 2
var dimension = 5
var initSize = 0.17

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  array = create2DArray(dimension, dimension, 0, false, 2)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize)
      fill(255)
      noStroke()
      rect(0, 0, boardSize * initSize * 0.9, boardSize * initSize * 0.9)
      pop()
      for (var k = 0; k < array[i][j].length; k++) {
        for (var l = 0; l < array[i][j][k].length; l++) {
          if (array[i][j][k][l] === 1) {
            push()
            translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize + (k - Math.floor(array[i][j].length * 0.5) + ((array[i][j].length + 1) % 2) * 0.5) * boardSize * initSize * (1 / array[i][j].length) * 0.8, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize + (l - Math.floor(array[i][j][k].length * 0.5) + ((array[i][j][k].length + 1) % 2) * 0.5) * boardSize * initSize * (1 / array[i][j][k].length) * 0.8)
            fill(0)
            noStroke()
            rect(0, 0, boardSize * initSize * (1 / array[i][j].length) * 0.8 + 1, boardSize * initSize * (1 / array[i][j][k].length) * 0.8 + 1)
            pop()
          }
        }
      }
    }
  }

  tileDimension = Math.floor(Math.random() * 4 + 1)
  var replace = Number(Math.floor(Math.random() * (Math.pow(2, tileDimension * tileDimension) - 1))).toString(2)
  var colLen = replace.length
  for (var k = 0; k < (tileDimension * tileDimension) - colLen; k++) {
    replace = '0' + replace
  }
  var subArr = create2DArray(tileDimension, tileDimension, 0, true)
  for (var k = 0; k < subArr.length; k++) {
    for (var l = 0; l < subArr.length; l++) {
      subArr[k][l] = parseInt(replace[k * tileDimension + l], 2)
    }
  }
  array[Math.floor(Math.random() * dimension)][Math.floor(Math.random() * dimension)] = subArr
}

function drawTile(type, size) {
  var binary = Number(type).toString(2)
  return binary
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool, p) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        tileDimension = p
        columns[j] = Number(Math.floor(Math.random() * (Math.pow(2, tileDimension * tileDimension) - 1))).toString(2)
        var colLen = columns[j].length
        for (var k = 0; k < (tileDimension * tileDimension) - colLen; k++) {
          columns[j] = '0' + columns[j]
        }
        var subArr = create2DArray(tileDimension, tileDimension, 0, true)
        for (var k = 0; k < subArr.length; k++) {
          for (var l = 0; l < subArr.length; l++) {
            subArr[k][l] = parseInt(columns[j][k * tileDimension + l], 2)
          }
        }
        columns[j] = subArr
      }
    }
    array[i] = columns
  }
  return array
}
